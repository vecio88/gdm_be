package it.michelepiazzolla.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.michelepiazzolla.entity.Articolo;
import it.michelepiazzolla.repository.ArticoliModel;

@Service
public class ArticoliServiceImpl implements ArticoliService {

	@Autowired
	ArticoliModel articoliModel;
	
	@Override
	public List<Articolo> selezionaTuttiGliArticoli() {
		
		return articoliModel.findAll();
	}

	@Override
	public void inserisciArticolo(Articolo articolo) {
		articoliModel.save(articolo);
		
	}

}
