package it.michelepiazzolla.services;

import java.util.List;

import it.michelepiazzolla.entity.Articolo;

public interface ArticoliService {
	public List<Articolo> selezionaTuttiGliArticoli();
	
	public void inserisciArticolo(Articolo articolo);
}
