package it.michelepiazzolla.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import it.michelepiazzolla.entity.Articolo;

@Repository
public interface ArticoliModel extends MongoRepository<Articolo, Long> {}
