package it.michelepiazzolla.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.michelepiazzolla.entity.Articolo;
import it.michelepiazzolla.services.ArticoliService;

@RestController
@RequestMapping("/articolo")
public class ArticoliController {
	
	@Autowired
	ArticoliService articoliService;
	
	@GetMapping(value = "/all", produces = "application/json")
	public ResponseEntity<List<Articolo>> getArticoli() {
		
		List<Articolo> lstArticoli = articoliService.selezionaTuttiGliArticoli();

		return new ResponseEntity<List<Articolo>>(lstArticoli, HttpStatus.CREATED);
	}

	@PostMapping("/inserisciArticolo")
	public String insArticolo(@RequestBody Articolo articolo) {
		articoliService.inserisciArticolo(articolo);
		return "Articolo inserito";
		
		
	}
}
