package it.michelepiazzolla;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionaleDiMagazzinoApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionaleDiMagazzinoApplication.class, args);
	}

}
